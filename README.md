# L'école des loustics

## Sujet
L’éducation nationale propose de vous engager pour réaliser une application mobile destinée aux écoles  primaires.  
Cette  application  permettra  aux  élèves  de  s’auto-évaluer sur  des  petits exercices mathématiqueset des exercicesà base de connaissances.<br>

Les fonctionnalités **minimales** de l’application seront: <br>

* Proposer des exercices mathématiques: dans un premier temps, table de multiplication et exercice proposant une série de 10 additions simplesà réaliser;
* Créer des comptes pour chaque élève (prénom et nom);
* Proposer  des exercicesde  culture  générale:dans  un premier  temps,  un  exercice  de  typequestion/réponseen français; <br>

Exemples  d’applications  réalisées  par  des  étudiants(le  projet  proposé  à  ces  étudiants  était légèrement différent mais l’idée était la même): <br>

[https://www.youtube.com/watch?v=ow--EYXRK9g](https://www.youtube.com/watch?v=ow--EYXRK9g)


## Application
Rapport.pdf regroupe les fonctionnalités et le workflow du projet.<br>

Tout le code source est développé avec android studio en Java.
