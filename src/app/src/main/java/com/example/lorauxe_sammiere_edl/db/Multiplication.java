package com.example.lorauxe_sammiere_edl.db;

public class Multiplication {

    private Operation[] table;

    public Multiplication(double mul) {

        table = new Operation[10];

        for (int i = 1; i <= 10; i++) {
            table[i-1] = new Operation(i, mul);
        }
    }


    public Operation getMultiplication(int n) {return table[n-1];}

    public double getResultMutiple(int n) {return table[n-1].getOperand1() * table[n-1].getOperand2();}

    public Operation[] getTable() {return table;}

}
