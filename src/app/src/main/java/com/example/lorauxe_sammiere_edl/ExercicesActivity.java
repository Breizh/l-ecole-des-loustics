package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ExercicesActivity extends AppCompatActivity {

    public static final String ACCOUNT_NAME_KEY = "accountname_key";
    private String accountName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercices);

//        Récupérer le nom du compte passé via l'intent, et définir la barre de titre
        accountName = getIntent().getStringExtra(ACCOUNT_NAME_KEY);
        setTitle(accountName);
    }

    public void onExMul(View view) {
        // Création d'une intention
        Intent ExMulViewActivityIntent = new Intent(this, ExMulActivity.class);

        // Lancement de la demande de changement d'activité
        startActivity(ExMulViewActivityIntent);
    }

    public void onAdd(View view) {
        // Création d'une intention
        Intent ExAddViewActivityIntent = new Intent(this, ExAddDificultyActivity.class);

        // Lancement de la demande de changement d'activité
        startActivity(ExAddViewActivityIntent);
    }

    public void onExFr(View view) {
        // Création d'une intention
        Intent ExFrIntent = new Intent(this, ExFrancaisActivity.class);

        // Lancement de la demande de changement d'activité
        startActivity(ExFrIntent);
    }
}