package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;

public class ExMulActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex_mul);

        NumberPicker table = findViewById(R.id.table_chose);
        table.setMaxValue(10);
        table.setMinValue(0);
    }

    public void goToTable(View view) {
        NumberPicker table = findViewById(R.id.table_chose);

        // Création d'une intention
        Intent tableViewActivityIntent = new Intent(this, ExMulTableActivity.class);

        tableViewActivityIntent.putExtra(ExMulTableActivity.TABLE_KEY, table.getValue());
        // Lancement de la demande de changement d'activité
        startActivity(tableViewActivityIntent);
    }
}