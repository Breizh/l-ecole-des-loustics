package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lorauxe_sammiere_edl.db.Account;
import com.example.lorauxe_sammiere_edl.db.DatabaseClient;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

//    Client pour la DB
    private DatabaseClient dbClient;
//    Adapteur pour la ListView des comptes
    private ArrayAdapter<Account> accountsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Initialiser le client d'accès à la DB
        dbClient = DatabaseClient.getInstance(getApplicationContext());

        //=================================== LISTVIEW =============================================

//        Récupérer la ListView des comptes depuis le layout
        ListView accountsListView = findViewById(R.id.accounts_list_view);

//        Créer et assigner l'ArrayAdapter à notre ListView
//        On lui passe une liste vide pour l'instant, la méthode getAccounts() appelée au lancement
//        de l'activité s'occupera de remplir l'adapteur
        accountsListAdapter = new ArrayAdapter<Account>(this, R.layout.simple_list_item, R.id.item_text, new ArrayList<Account>());
        accountsListView.setAdapter(accountsListAdapter);


//        Listener pour les clics sur les éléments de la liste
        AdapterView.OnItemClickListener accountClickedHandler = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Récupérer le nom du compte depuis la vue
                String accountName = (String) ((TextView) view).getText();
                start(accountName);
            }
        };
        accountsListView.setOnItemClickListener(accountClickedHandler);

        //=================================== BOUTONS DU BAS =======================================
        Button btnCreateAccount = findViewById(R.id.btn_create_account);
        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createAccountIntent = new Intent(MainActivity.this, CreateAccountActivity.class);
                startActivity(createAccountIntent);
            }
        });

        Button btnAnonymous = findViewById(R.id.btn_anonymous);
        btnAnonymous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start(getString(R.string.anonymous));
            }
        });

    }

    private void start(String accountName) {
//        Petit message "toast" de confirmation
        Toast.makeText(MainActivity.this, String.format(getString(R.string.login_message),accountName), Toast.LENGTH_SHORT).show();
//        Lancer l'activité des exercices
        Intent exMulActivityIntent = new Intent(MainActivity.this, ExercicesActivity.class);
        exMulActivityIntent.putExtra(ExercicesActivity.ACCOUNT_NAME_KEY, accountName);
        startActivity(exMulActivityIntent);
    }

    private void getAccounts() {
        ///////////////////////
        // Classe asynchrone permettant de récupérer les comptes et de mettre à jour la ListView de l'activité
        // L'asynchrone est nécessaire pour éviter de figer la vue pendant les accès à la DB
        class GetAccounts extends AsyncTask<Void, Void, List<Account>> {

            @Override
            protected List<Account> doInBackground(Void... voids) {
                return dbClient.getAppDatabase()
                        .accountDAO()
                        .getAll();
            }

            @Override
            protected void onPostExecute(List<Account> accounts) {
                super.onPostExecute(accounts);

                // Mettre à jour l'adapter avec la liste de comptes
                accountsListAdapter.clear();
                accountsListAdapter.addAll(accounts);

                // ..et le notifier du changement
                accountsListAdapter.notifyDataSetChanged();
            }
        }

        //////////////////////////
        // Maintenant que la classe existe, on peut l'instancier et exécuter la tâche asynchrone
        GetAccounts gAcc = new GetAccounts();
        gAcc.execute();
    }


    @Override
    protected void onStart() {
        super.onStart();

        // Mise à jour des comptes
        getAccounts();

    }
}