package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class CongratulationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulations);
    }


    public void onExercices(View view) {

        // Création d'une intention
        Intent onExercicesActivity = new Intent(this, ExercicesActivity.class);

        onExercicesActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Lancement de la demande de changement d'activité
        startActivity(onExercicesActivity);
    }
}