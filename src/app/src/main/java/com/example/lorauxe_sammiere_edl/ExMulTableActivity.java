package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lorauxe_sammiere_edl.db.Multiplication;
import com.example.lorauxe_sammiere_edl.db.Operation;

import java.util.HashMap;

import static java.lang.Integer.parseInt;

public class ExMulTableActivity extends AppCompatActivity {

    public static final String TABLE_KEY = "table_key";
    private int mul;
    private Multiplication multiplications;
    private HashMap<Integer ,EditText> listMultiplication = new HashMap<Integer ,EditText>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex_mul_table);

        LinearLayout linear = findViewById(R.id.ExMulLayoutTable);

        // retrieving the value of the table
        this.mul = getIntent().getIntExtra(TABLE_KEY,0);

        double d = this.mul;
        this.multiplications = new Multiplication(d);

        // generated of multiplication
        for (Operation op : multiplications.getTable()) {
            // temporary layout
            LinearLayout linearTML = new LinearLayout(this);
            linearTML.setOrientation(LinearLayout.HORIZONTAL);

            // Text field of mutiplication
            TextView calcul = new TextView(this);
            calcul.setText(String.format("%d x %d = ", (int) op.getOperand1(), (int) op.getOperand2()));

            // Edit text of answer
            EditText answer = new EditText(this);
            answer.setInputType(InputType.TYPE_CLASS_NUMBER);
            answer.setHint("?");

            listMultiplication.put((int)op.getOperand1(), answer);

            // add at layout
            linearTML.addView(calcul);
            linearTML.addView(answer);

            linear.addView(linearTML);
        }
    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }


    public void verif(View view) {
        int nb_error = 0;
        for (Operation op : multiplications.getTable()) {
            if(isNumeric(listMultiplication.get((int) op.getOperand1()).getText().toString())){
                if (parseInt(
                        listMultiplication.get((int) op.getOperand1()).getText().toString()) != multiplications.getResultMutiple((int) op.getOperand1())
                )
                {nb_error++;}
            }
            else {nb_error++;}

        }

        if(nb_error != 0){
            // Création d'une intention
            Intent erreurViewActivityIntent = new Intent(this, ErrorActivity.class);

            erreurViewActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            erreurViewActivityIntent.putExtra(ErrorActivity.NBERREUR_KEY, nb_error);
            // Lancement de la demande de changement d'activité
            startActivity(erreurViewActivityIntent);
        }
        else {
            // Création d'une intention
            Intent felicitationViewActivityIntent = new Intent(this, CongratulationsActivity.class);

            felicitationViewActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Lancement de la demande de changement d'activité
            startActivity(felicitationViewActivityIntent);
        }

    }
}