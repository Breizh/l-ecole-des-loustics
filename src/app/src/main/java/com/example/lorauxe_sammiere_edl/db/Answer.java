package com.example.lorauxe_sammiere_edl.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(
        tableName = "answers",
//        questionId est une clé étrangère vers les ID de Question,
//        qui indique à quelle question appartient cette réponse
        foreignKeys = {@ForeignKey(
                entity = Question.class,
                parentColumns = "questionId",
                childColumns = "parentQuestionId",
                onDelete = ForeignKey.CASCADE)}
)
public class Answer implements Serializable {

    @PrimaryKey
    private long answerId;

    @ColumnInfo
    private long parentQuestionId;

    @ColumnInfo
    private String text;

    @ColumnInfo
    private boolean correct;

//    == Getters / setters ==

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public long getParentQuestionId() {
        return parentQuestionId;
    }

    public void setParentQuestionId(long parentQuestionId) {
        this.parentQuestionId = parentQuestionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }
}
