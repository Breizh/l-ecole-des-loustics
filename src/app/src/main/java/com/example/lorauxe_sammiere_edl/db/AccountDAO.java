package com.example.lorauxe_sammiere_edl.db;



import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface AccountDAO {

    @Query("SELECT * FROM account")
    List<Account> getAll();

//  === Opérations classiques par défaut ===
    @Insert
    void insert(Account account);

    @Insert
    long[] insertAll(Account... accounts);

    @Delete
    void delete(Account account);

    @Update
    void update(Account account);

}