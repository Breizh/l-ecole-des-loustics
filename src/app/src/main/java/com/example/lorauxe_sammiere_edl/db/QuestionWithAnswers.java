package com.example.lorauxe_sammiere_edl.db;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class QuestionWithAnswers {
//    Cette classe représente la relation 1-N entre les questions et les réponses
//    On utilise un mécanisme intégré à Room pour récupérer des données de plusieurs tables
//    dans un seul objet
//    (cf. AnswerDAO)

    @Embedded public Question question;

    @Relation(
            parentColumn = "questionId",
            entityColumn = "parentQuestionId"
    )
    public List<Answer> answers;
}
