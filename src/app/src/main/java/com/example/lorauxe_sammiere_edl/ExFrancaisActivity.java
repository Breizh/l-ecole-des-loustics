package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lorauxe_sammiere_edl.db.Answer;
import com.example.lorauxe_sammiere_edl.db.DatabaseClient;
import com.example.lorauxe_sammiere_edl.db.QuestionWithAnswers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExFrancaisActivity extends AppCompatActivity {

    private DatabaseClient dbClient;

    private static final int QUESTION_COUNT = 10;
    private List<QuestionWithAnswers> exQuestions;
    private int currQuestionIndex;
    private QuestionWithAnswers currQuestion;

    private int successCount = 0;
    private int errorCount = 0;

//    == Elements graphiques ==
    private LinearLayout mainLayout;
    private TextView questionNumberView;
    private TextView questionTextView;
    private Button buttonNext;
    private LinearLayout answersLayout;
    private List<CheckBox> answersCheckboxes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex_francais);
//        Définir le titre de l'activité
        setTitle(R.string.ExFr);

//        == Récupération des éléments graphiques ==
        mainLayout = findViewById(R.id.francais_linearlayout);
        questionNumberView = findViewById(R.id.question_number);
        questionTextView = findViewById(R.id.question_text);
        buttonNext = findViewById(R.id.question_act_button_next);
        answersLayout = findViewById(R.id.answers_layout);
        answersCheckboxes = new ArrayList<>();
//        ==========================================
//        Récupérer la base de données globale
        dbClient = DatabaseClient.getInstance(getApplicationContext());

//        Récupérer les questions
        loadQuestions();
    }

//    Comme d'habitude, on doit créer une méthode contenant une classe tâche asynchrone
//    pour interroger la DB sans figer l'UI
    private void loadQuestions() {

        class GetQuestions extends AsyncTask<Void, Void, List<QuestionWithAnswers>> {
            @Override
            protected List<QuestionWithAnswers> doInBackground(Void... voids) {
                return dbClient
                        .getAppDatabase()
                        .answerDAO()
                        .getQuestionsWithAnswers();
            }

            @Override
            protected void onPostExecute(List<QuestionWithAnswers> questionsWithAnswers) {
                super.onPostExecute(questionsWithAnswers);
                setQuestions(questionsWithAnswers);
            }
        }
        GetQuestions getQuestions = new GetQuestions();
        getQuestions.execute();
    }


    private void setQuestions(List<QuestionWithAnswers> questions) {
//        Attention aux noms :
//        "questions" est la liste de toutes les questions récupérées en base de données,
//        "exQuestions" est la liste de questions de l'exercice (pour l'instant vide)
        exQuestions = new ArrayList<>();

//        Mélanger la liste de questions pour les tirer au hasard
        Collections.shuffle(questions);

//        Remplir la liste de questions de l'exercice avec <QUESTION_COUNT> questions,
//        ou moins si la base de données n'en contient pas assez
        for (int i=0; i < Math.min(questions.size(),QUESTION_COUNT); i++) {
            exQuestions.add(questions.get(i));
        }

        showQuestion();
//        Rendre le bouton Suivant visible
        buttonNext.setVisibility(View.VISIBLE);
    }

    private void showQuestion() {
//        Récupérer la question correspondant à l'index
        currQuestion = exQuestions.get(currQuestionIndex);

//        Mettre à jour le texte de la question, et son numéro
        questionTextView.setText(currQuestion.question.getText());
        questionNumberView.setText(String.format(
                getString(R.string.question_number),
                currQuestionIndex,
                exQuestions.size()));

//        Vider les réponses de l'ancienne question
        answersCheckboxes.clear();
        answersLayout.removeAllViews();
//        Remplir les nouvelles réponses
        Collections.shuffle(currQuestion.answers);// Les réponses seront dans un ordre aléatoire
        for (Answer ans : currQuestion.answers) {
            CheckBox questionCheckbox = new CheckBox(getApplicationContext());
            questionCheckbox.setText(ans.getText());
//            Ajouter la checkbox à la vue..
            answersLayout.addView(questionCheckbox);
//            .. et à notre liste de checkbox interne pour vérification
            answersCheckboxes.add(questionCheckbox);
        }
    }

    public void nextQuestion(View view) {
//        Vérifier si cette question est réussie
        verifyQuestion();
//        Passer à la question suivante
        currQuestionIndex++;
        if (currQuestionIndex <= exQuestions.size()-1) {
//            On est encore dans une intervalle valide, la question existe
//            Pour la dernière question, on change le texte du bouton
            if (currQuestionIndex == exQuestions.size()-1) {
                buttonNext.setText(R.string.button_finish);
            }
            showQuestion();
        } else {
//            /!\ On est arrivé à la fin des questions
            Toast.makeText(this, String.format("Terminé ! %d succès, %d erreurs",successCount,errorCount), Toast.LENGTH_SHORT).show();
            Intent qcmResultIntent = new Intent(this, ExQCMResultActivity.class);
            qcmResultIntent.putExtra(ExQCMResultActivity.SUCCESS_COUNT_KEY, successCount);
            qcmResultIntent.putExtra(ExQCMResultActivity.ERROR_COUNT_KEY, errorCount);
            startActivity(qcmResultIntent);
        }
    }

    
    private void verifyQuestion() {
        int i=0;
//        On parcourt toutes les réponses en vérifiant si l'état de la checkbox correspond bien
//        à la nature de la réponse (juste ou fausse)
        while (i < currQuestion.answers.size()
                && answersCheckboxes.get(i).isChecked() == currQuestion.answers.get(i).isCorrect()) {
            i++;
        }
        if (i >= currQuestion.answers.size()) {
//            On est arrivé au bout, toutes les réponses sont correctes : la question est réussie
            successCount++;
        } else {
//            On est tombé sur une réponse incorrecte : la question est ratée
            errorCount++;
        }
    }
}