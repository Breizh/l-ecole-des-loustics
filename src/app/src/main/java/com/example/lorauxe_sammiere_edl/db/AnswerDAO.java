package com.example.lorauxe_sammiere_edl.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface AnswerDAO {

//    Avec la relation définie dans la classe QuestionsWithAnswers,
//    on peut récupérer les questions et leurs N réponses
//    en un seul SELECT
//    (cf. https://developer.android.com/training/data-storage/room/relationships#one-to-many )
//    Pour une raison obscure, cette requête doit être dans la classe DAO *de l'entité fille*,
//    c'est à dire Answer dans notre cas, même si elle renvoie plutôt des Questions contenant des Answers
    @Transaction
    @Query("SELECT * FROM question")
    List<QuestionWithAnswers> getQuestionsWithAnswers();

//  === Opérations classiques par défaut ===
    @Query("SELECT * FROM answers")
    List<Answer> getAll();

    @Insert
    void insert(Answer answer);

    @Insert
    long[] insertAll(Answer... answers);

    @Delete
    void delete(Answer answer);

    @Update
    void update(Answer answer);
    
}
