package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ExAddDificultyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex_add_dificulty);
    }

    public void onEasy(View view) {
        // Création d'une intention
        Intent ExAddViewActivityIntent = new Intent(this, AddActivity.class);
        int min = 0;
        int max = 100;

        ExAddViewActivityIntent.putExtra(AddActivity.MIN_KEY, min);
        ExAddViewActivityIntent.putExtra(AddActivity.MAX_KEY, max);

        // Lancement de la demande de changement d'activité
        startActivity(ExAddViewActivityIntent);
    }

    public void onMedium(View view) {
        // Création d'une intention
        Intent ExAddViewActivityIntent = new Intent(this, AddActivity.class);

        ExAddViewActivityIntent.putExtra(AddActivity.MIN_KEY, 0);
        ExAddViewActivityIntent.putExtra(AddActivity.MAX_KEY, 500);

        // Lancement de la demande de changement d'activité
        startActivity(ExAddViewActivityIntent);
    }

    public void onHard(View view) {
        // Création d'une intention
        Intent ExAddViewActivityIntent = new Intent(this, AddActivity.class);

        ExAddViewActivityIntent.putExtra(AddActivity.MIN_KEY, 0);
        ExAddViewActivityIntent.putExtra(AddActivity.MAX_KEY, 1000);

        // Lancement de la demande de changement d'activité
        startActivity(ExAddViewActivityIntent);
    }
}