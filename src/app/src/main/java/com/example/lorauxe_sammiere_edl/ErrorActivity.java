package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ErrorActivity extends AppCompatActivity {

    public static final String NBERREUR_KEY = "nombre_erreur_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);


        int nberror = getIntent().getIntExtra(NBERREUR_KEY,0);

        TextView nberreur = findViewById(R.id.nberror);
        nberreur.setText(String.format(getString(R.string.nberror), nberror));
    }

    public void onExercices(View view) {

        // Création d'une intention
        Intent onExercicesActivity = new Intent(this, ExercicesActivity.class);

        onExercicesActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Lancement de la demande de changement d'activité
        startActivity(onExercicesActivity);
    }

    public void onCorrection(View view) {
        finish();
    }
}