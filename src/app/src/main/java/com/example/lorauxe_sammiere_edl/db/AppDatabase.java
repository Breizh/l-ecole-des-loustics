package com.example.lorauxe_sammiere_edl.db;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(
        entities = {Account.class,Question.class,Answer.class},
        version = 1,
        exportSchema = false
)
public abstract class AppDatabase extends RoomDatabase {

    public abstract AccountDAO accountDAO();
    public abstract QuestionDAO questionDAO();
    public abstract AnswerDAO answerDAO();

}