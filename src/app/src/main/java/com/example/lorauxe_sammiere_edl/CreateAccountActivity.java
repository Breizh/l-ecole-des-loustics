package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.lorauxe_sammiere_edl.db.Account;
import com.example.lorauxe_sammiere_edl.db.DatabaseClient;

import java.util.List;

public class CreateAccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        EditText textboxFirstname = findViewById(R.id.textbox_firstname);
        EditText textboxLastname = findViewById(R.id.textbox_lastname);

        Button createBtn = findViewById(R.id.btn_create_account);
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Il faudrait faire l'insertion en base ici, mais pas le temps, dommage...
            }
        });
    }
}