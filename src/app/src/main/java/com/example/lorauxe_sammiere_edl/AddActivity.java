package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.lorauxe_sammiere_edl.db.Operation;

import java.util.Random;

import static java.lang.Integer.parseInt;

public class AddActivity extends AppCompatActivity {

    public static final String MAX_KEY = "max_key";
    public static final String MIN_KEY = "min_key";
    private int compteur;
    private int nb_error;
    private Operation[] opList = new Operation[10];
    private int[] answerList = new int[10];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        compteur = 1;
        nb_error = 0;

        int max = getIntent().getIntExtra(MAX_KEY,0);
        int min = getIntent().getIntExtra(MIN_KEY,0);

        System.out.println(max + " + " + min);
        // Génération des operation et initialisation d'es reponse a -1
        for (int i = 0; i < 10; i++) {
            Operation op = new Operation((double)getRandomNumberInRange(min, max), (double)getRandomNumberInRange(min, max));

            opList[i] = op;
            answerList[i] = -1;
        }


        // Mise a jours graphique
        TextView textCompteur = findViewById(R.id.addCompteur);
        textCompteur.setText(compteur + "/10");

        TextView addition = findViewById(R.id.addition);
        addition.setText((int)opList[compteur-1].getOperand1() + " + " + (int)opList[compteur-1].getOperand2() + " = ");
    }

    private static int getRandomNumberInRange(int min, int max) {
        // {min < max} => {retourne un int aléatoire entre min et max}

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static boolean isNumeric(String str) {
        // {} => {return vrai si la string est un entier et false sinon}
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }


    public void next(View view) {
        EditText answer = findViewById(R.id.addAnswer);
        String answerText = answer.getText().toString();

        if (compteur < 10){

            // Si réponse valide mise a jours dans la liste des reponse sinon -1
            if (isNumeric(answerText)){
                answerList[compteur-1] = parseInt(answerText);
            }
            else {
                answerList[compteur-1] = -1;
            }

            compteur++;

            // Mise a jour graphique
            TextView textCompteur = findViewById(R.id.addCompteur);
            textCompteur.setText(String.format("%d/10", compteur));

            TextView addition = findViewById(R.id.addition);
            addition.setText(String.format("%d + %d = ", (int)opList[compteur-1].getOperand1(), (int)opList[compteur-1].getOperand2()));

            // Si reponse déjà set, affichage de celle ci
            if (answerList[compteur-1] >= 0){answer.setText(Integer.toString(answerList[compteur-1]));}
            else {answer.setText("");}
        }

        else {

            // Comptage des erreurs
            for (int i = 0; i < 10; i++) {
                if((int)opList[i].getOperand1() + (int)opList[i].getOperand2() != answerList[i]){
                    nb_error++;
                }
            }

            if(nb_error != 0){
                // Création d'une intention en cas d'erreur
                Intent erreurViewActivityIntent = new Intent(this, ErrorActivity.class);

                erreurViewActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                erreurViewActivityIntent.putExtra(ErrorActivity.NBERREUR_KEY, nb_error);
                // Lancement de la demande de changement d'activité
                startActivity(erreurViewActivityIntent);
            }
            else {
                // Création d'une intention en cas de reusite
                Intent felicitationViewActivityIntent = new Intent(this, CongratulationsActivity.class);

                felicitationViewActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                // Lancement de la demande de changement d'activité
                startActivity(felicitationViewActivityIntent);
            }
        }
    }

    public void prev(View view) {
        if (compteur > 1){
            EditText answer = findViewById(R.id.addAnswer);
            String answerText = answer.getText().toString();

            // Si réponse valide mise a jours dans la liste des reponse sinon -1
            if (isNumeric(answerText)){answerList[compteur-1] = parseInt(answerText);}
            else {answerList[compteur-1] = -1;}

            compteur--;

            // Si réponse valide mise a jours dans la liste des reponse sinon -1
            // Mise a jour graphique
            TextView textCompteur = findViewById(R.id.addCompteur);
            textCompteur.setText(compteur + "/10");

            TextView addition = findViewById(R.id.addition);
            addition.setText((int)opList[compteur-1].getOperand1() + " + " + (int)opList[compteur-1].getOperand2() + " = ");

            // Si reponse déjà set, affichage de celle ci
            if (answerList[compteur-1] >= 0){answer.setText(Integer.toString(answerList[compteur-1]));}
            else {answer.setText("");}
        }
    }
}