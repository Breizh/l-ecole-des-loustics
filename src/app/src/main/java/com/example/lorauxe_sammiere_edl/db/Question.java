package com.example.lorauxe_sammiere_edl.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.List;

@Entity
public class Question implements Serializable {
    @PrimaryKey
    private long questionId;

    @ColumnInfo // la colonne prend par défaut le nom de l'attribut, pas besoin d'en spécifier un
    private String text;

    @ColumnInfo
    private String subject;

    //    == Getters / setters ==
    public long getQuestionId() {
        return questionId;
    }

    public String getText() {
        return text;
    }

    public String getSubject() {
        return subject;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
