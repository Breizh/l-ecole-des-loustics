package com.example.lorauxe_sammiere_edl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ExQCMResultActivity extends AppCompatActivity {

    public static final String SUCCESS_COUNT_KEY = "success_count_key";
    public static final String ERROR_COUNT_KEY = "error_count_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex_qcm_result);

        int successCount = getIntent().getIntExtra(SUCCESS_COUNT_KEY,0);
        int errorCount = getIntent().getIntExtra(ERROR_COUNT_KEY,0);

        TextView qcm_text = findViewById(R.id.qcm_result_text);
        qcm_text.setText(String.format("Vous avez %d réponses correctes et %d réponses fausses",successCount,errorCount));
    }
    public void onExercices(View view) {

        // Création d'une intention
        Intent onExercicesActivity = new Intent(this, ExercicesActivity.class);

        onExercicesActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Lancement de la demande de changement d'activité
        startActivity(onExercicesActivity);
    }
}