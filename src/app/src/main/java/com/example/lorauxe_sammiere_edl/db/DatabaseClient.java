package com.example.lorauxe_sammiere_edl.db;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

public class DatabaseClient {

    // Instance unique permettant de faire le lien avec la base de données
    private static DatabaseClient instance;

    // Objet représentant la base de données de votre application
    private AppDatabase appDatabase;

    // Constructeur
    private DatabaseClient(final Context context) {

        // Créer l'objet représentant la base de données de votre application
        // à l'aide du "Room database builder"
        // EDLData est le nom de la base de données
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, "EDLData").build();

        ////////// REMPLIR LA BD à la première création à l'aide de l'objet roomDatabaseCallback
        // Ajout de la méthode addCallback permettant de populate (remplir) la base de données à sa création
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, "EDLData").addCallback(roomDatabaseCallback).build();
    }

    // Méthode statique
    // Retourne l'instance de l'objet DatabaseClient
    public static synchronized DatabaseClient getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseClient(context);
        }
        return instance;
    }

    // Retourne l'objet représentant la base de données de votre application
    public AppDatabase getAppDatabase() {
        return appDatabase;
    }

    // Objet permettant de populate (remplir) la base de données à sa création
    RoomDatabase.Callback roomDatabaseCallback = new RoomDatabase.Callback() {

        // Called when the database is created for the first time.
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            // Jeu de données de test
            db.execSQL("INSERT INTO account (lastname, firstname) VALUES(\"Lagaffe\", \"Gaston\");");
            db.execSQL("INSERT INTO account (lastname, firstname) VALUES(\"De Chez Smith\", \"Jules\");");
            db.execSQL("INSERT INTO account (lastname, firstname) VALUES(\"Demesmaeker\", \"Monsieur\");");


            db.execSQL("INSERT INTO question VALUES(0, '\"On ne peut plus rentrer en bus.\" - De quelle forme est cette phrase ?', 'fr')");
            db.execSQL("INSERT INTO answers VALUES(0, 0, 'Affirmative', 1)");
            db.execSQL("INSERT INTO answers VALUES(1, 0, 'Négative', 1)");
            db.execSQL("INSERT INTO answers VALUES(2, 0, 'Interrogative', 0)");
            db.execSQL("INSERT INTO answers VALUES(3, 0, 'Interro-négative', 0)");

            db.execSQL("INSERT INTO question VALUES(1, 'Le mot \"arrivait\" est...', 'fr')");
            db.execSQL("INSERT INTO answers VALUES(4, 1, 'un verbe', 1)");
            db.execSQL("INSERT INTO answers VALUES(5, 1, 'un nom', 0)");
            db.execSQL("INSERT INTO answers VALUES(6, 1, 'un adjectif', 0)");
            db.execSQL("INSERT INTO answers VALUES(7, 1, 'un déterminant', 0)");

            db.execSQL("INSERT INTO question VALUES(2, 'Les mots \"vert\" et \"verre\" sont des ...', 'fr')");
            db.execSQL("INSERT INTO answers VALUES(8, 2, 'homonymes', 1)");
            db.execSQL("INSERT INTO answers VALUES(9, 2, 'antonymes', 0)");
            db.execSQL("INSERT INTO answers VALUES(10, 2, 'synonymes', 0)");

            db.execSQL("INSERT INTO question VALUES(3, 'Lesquels de ces mots ont un contraire formable avec le préfixe \"in-\" ?', 'fr')");
            db.execSQL("INSERT INTO answers VALUES(11, 3, 'Correct', 1)");
            db.execSQL("INSERT INTO answers VALUES(12, 3, 'Exact', 1)");
            db.execSQL("INSERT INTO answers VALUES(13, 3, 'Connu', 1)");
            db.execSQL("INSERT INTO answers VALUES(14, 3, 'Possible', 0)");
            db.execSQL("INSERT INTO answers VALUES(15, 3, 'Réel', 0)");

            db.execSQL("INSERT INTO question VALUES(4, 'Quel est le pluriel de \"cheval\" ?', 'fr')");
            db.execSQL("INSERT INTO answers VALUES(16, 4, 'Chevaux', 1)");
            db.execSQL("INSERT INTO answers VALUES(17, 4, 'Chevaus', 0)");
            db.execSQL("INSERT INTO answers VALUES(18, 4, 'Chevals', 0)");


        }
    };
}
